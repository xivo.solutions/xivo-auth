FROM python:3.11-bookworm
MAINTAINER XiVO Dev Team <randd@xivo.solutions>

RUN apt-get -yq update \
   && apt-get -yqq dist-upgrade \
   && apt-get -yq autoremove \
   && apt-get -y install libldap2-dev \
                      libsasl2-dev \
                      git \
                      gcc \
   && rm -rf /var/lib/apt/lists/*

# Install
ADD . /usr/src/xivo-auth
WORKDIR /usr/src/xivo-auth
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN python3 setup.py install

#Configure environment
RUN touch /var/log/xivo-auth.log
RUN mkdir -p /etc/xivo-auth/conf.d
RUN cp /usr/src/xivo-auth/etc/xivo-auth/*.yml /etc/xivo-auth/
RUN install -d -o www-data -g www-data /var/run/xivo-auth/

ADD ./contribs/docker/certs /usr/share/xivo-certs

EXPOSE 9497

CMD ["xivo-auth", "-fd", "--user", "www-data"]
