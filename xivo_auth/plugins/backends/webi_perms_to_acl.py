# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# Foolowing is a dict that gives the mapping 
#   for a webi path (as constructed by the webi user acl object)
#   to a confd acl
WEBI_PERMS_TO_CONFD_ACL = {
    'acl.service.ipbx.pbx_settings.labels': 'confd.labels.#',
    'acl.service.ipbx.pbx_settings.users': 'confd.labels.read'
}
